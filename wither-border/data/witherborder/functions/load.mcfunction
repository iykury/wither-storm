scoreboard objectives add witherborder dummy
scoreboard objectives add divisor dummy
scoreboard objectives add cooldown dummy
scoreboard objectives add canContract dummy
scoreboard players add #border witherborder 0
scoreboard players set #border divisor 8
scoreboard players set #border cooldown 0
execute if score #border witherborder matches 0 run function witherborder:genesis
gamerule spawnRadius 1000

scoreboard objectives add respawnHealth health
scoreboard objectives add respawned dummy
scoreboard objectives add respawnRandomize dummy
scoreboard objectives add hasSpawn dummy
scoreboard objectives add inDimension dummy