execute as @a store success score @s hasSpawn run data get entity @s SpawnX
execute as @a store result score @s inDimension run data get entity @s Dimension

scoreboard players set @a[scores={respawnHealth=0}] respawned 0
scoreboard players set @a[scores={inDimension=1}] respawned 0
scoreboard players add @a respawned 0

scoreboard players set @a[scores={respawned=0,respawnHealth=1..,hasSpawn=0,inDimension=0}] respawnRandomize 1
execute as @a[scores={respawnRandomize=1}] run tp @s @r[scores={respawned=1},x=-1000,y=0,z=-1000,dx=2000,dy=2000,dz=2000]

scoreboard players set @a[scores={respawned=0,respawnRandomize=1}] respawned 1
scoreboard players set @a[scores={respawnRandomize=1}] respawnRandomize 0

execute unless score #border witherborder matches ..1 run scoreboard players remove #border witherborder 1
execute unless score #border cooldown matches ..0 run scoreboard players remove #border cooldown 1
execute if score #border witherborder matches ..1 if score #border cooldown matches ..0 run function witherborder:contract