execute store success score #border canContract run worldborder add -2 2
execute if score #border canContract matches 0 run worldborder set 2 2
scoreboard players set #border cooldown 40

execute store result score #border witherborder run worldborder get
scoreboard players operation #border witherborder /= #border divisor