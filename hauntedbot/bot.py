#!/usr/bin/env python3
import discord, os, asyncio, io, time, random

client = discord.Client()

filepath = os.path.abspath(__file__)
folder = os.path.dirname(filepath)
os.chdir(folder)

config = {}
try:
	with open("config") as configFile:
		for line in configFile.readlines():
			splitLine = line.split("=", 1)
			if len(splitLine) == 2:
				config[splitLine[0]] = splitLine[1].strip("\n")
except:
	print("Couldn't read config file")


token = config.get("token")
if token is None:
	print("Couldn't get token")

try:
	channelId = int(config.get("channelId"))
except:
	print("Couldn't get channel id")

channel = None

@client.event
async def on_ready():
	print("Logged in as")
	print(client.user.name)
	print(client.user.id)
	print("------")

	global channel
	channel = client.get_channel(channelId)
	voice = await channel.connect()
	genTask = None
	playTask = None
	while True:
		#If the bot is the only one connected, don't play any audio
		if channel.members == [channel.guild.me]:
			if not genTask is None and not genTask.cancelled():
				genTask.cancel()
				playTask.cancel()
		else:
			if genTask is None or genTask.cancelled():
				rawAudio = io.BytesIO()
				genTask = asyncio.create_task(genAudio(rawAudio))
				await asyncio.sleep(1)
				playTask = asyncio.create_task(play(voice, rawAudio))
		await asyncio.sleep(1)

async def play(voice, stream):
	voice.play(discord.PCMAudio(stream))
	
async def genAudio(stream):
	start = time.time()
	interval = 960 #Number of samples to read and write at a time (960 samples = 20 ms)
	trackFiles = []
	for i in range(4):
		trackFiles.append(open("screams/" + random.choice(os.listdir("screams")), "rb"))
	totalSamp = 0 #Number of samples written in total

	while True:
		elapsed = 48000 * (time.time() - start)
		if elapsed > totalSamp - 48000 * 5: #Make sure there's always 5 seconds of buffer audio
			tracks = []
			for f in range(len(trackFiles)):
				track = []
				raw = trackFiles[f].read(interval * 4)

				#If the end of the file is reached, pad with nulls and load a new file
				if len(raw) < interval * 4:
					padding = b'\x00' * (interval * 4 - len(raw))
					raw += padding
					trackFiles[f] = open("screams/" + random.choice(os.listdir("screams")), "rb")

				#Convert raw sample data into an array of integers
				for sample in range(interval * 2):
					#       Lower byte                    Upper byte
					value = raw[sample * 2] + raw[sample * 2 + 1] * 256
					if value > 32767:
						value -= 65536
					track.append(value)
				tracks.append(track)

			#Add samples together
			samples = []
			for sample in range(interval * 2):
				value = 0
				for track in tracks:
					value += track[sample]
				if value > 32767:
					value = 32767
				if value < -32768:
					value = -32768
				samples.append(value)
			
			#Convert array of integers into raw sample data
			rawSamples = b''
			for sample in samples:
				value = sample
				if value < 0:
					value += 65536
				#                        Lower byte   Upper byte
				rawSamples += bytearray([value % 256, int(value / 256)])

			#Seek to the end, write the data, then seek back
			seek = stream.tell()
			stream.seek(totalSamp * 4)
			stream.write(rawSamples)
			stream.seek(seek)

			totalSamp += interval
		else:
			await asyncio.sleep(1) #If there's enough of a buffer, wait 1 second

client.run(token)
