#!/usr/bin/env python3
import time, mcrcon, os, math, re, random

filepath = os.path.abspath(__file__)
folder = os.path.dirname(filepath)
os.chdir(folder)

config = {}
try:
	with open("config") as configFile:
		for line in configFile.readlines():
			splitLine = line.split("=", 1)
			if len(splitLine) == 2:
				config[splitLine[0]] = splitLine[1].strip("\n")
except:
	print("Couldn't read config file")

logfile = config.get("logfile")
if logfile is None:
	print("Couldn't get logfile")

rconHost = config.get("rconHost")
if rconHost is None:
	print("Couldn't get RCON hostname")

rconPass = config.get("rconPass")
if rconPass is None:
	print("Couldn't get RCON password")

rconPort = config.get("rconPort")
if rconPort is None:
	rconPort = 25575

def cmd(command):
	now = time.gmtime()
	print("[{}] {}".format(time.strftime("%H:%M:%S"), command))
	resp = mcr.command(command)
	print(resp)
	return resp

def getLines(filename):
	with open(filename, "r") as f:
		lines = f.readlines()
	if lines[-1:] == "\n":
		lines = lines[:-1]
	return lines

with mcrcon.MCRcon(rconHost, rconPass, rconPort) as mcr:
	connected = True
	delay = 1
	new = getLines(logfile)
	stopPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: Stopping server$")
	cmdPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: <.+> distance$")
	rconPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[RCON Listener #\\d+/INFO]: RCON running on \\d+\\.\\d+\\.\\d+\\.\\d+:\\d+$")
	while True:
		old = new
		try:
			new = getLines(logfile)
		except FileNotFoundError:
			pass
		if len(new) > len(old):
			newLines = new[len(old):]
		elif len(new) < len(old):
			newLines = new
		else:
			newLines = []

		for line in newLines:
			if connected:
				#If the server is shutting down, disconnect
				if not stopPattern.match(line) is None:
					mcr.disconnect()
					connected = False
				#If a player types "!distance" in chat, calculate their distance and send it to them
				if not cmdPattern.match(line) is None:
					name = line[34:-11]
					#Get coordinates with /data and split the relevant part of the response into X, Y, and Z coordinates
					resp = cmd("data get entity {} Pos".format(name))
					coords = resp[33+len(name):-1].split(", ")
					#Get dimension 
					resp = cmd("data get entity {} Dimension".format(name))
					dimension = int(resp[32+len(name):])
					#If in the end, output random mathematical nonsense
					if dimension == 1:
						nonsenseTypes = [
							"a/0", #Division by 0
							"an", #Coefficient and variable
							"ai", #Imaginary number
							"0^0", #Zero to the power of zero
						]

						a = str(random.randrange(-16, 17))
						if random.choice([True, False]):
							b = str(random.randrange(-16, 17))
							if random.choice([True, False]):
								if b == "0":
									bHalf = random.choice(["", "-"]) + "½"
								else:
									bHalf = b + "½"
							else:
								bHalf = b
						else:
							b = None
							bHalf = None

						nType = random.choice(nonsenseTypes)
						if nType == "a/0":
							distanceStr = a + "÷0"
							if not b is None:
								distanceStr = bHalf + "+" + distanceStr
						elif nType == "an":
							variable = random.choice("abcdefghjklmnopqrstuvwxyz")
							if a == "1":
								distanceStr = variable
							elif a == "-1":
								distanceStr = "-" + variable
							else:
								distanceStr = a + variable
							if not b is None:
								distanceStr += "+" + b
						elif nType == "ai":
							if a == "1":
								distanceStr = "i"
							elif a == "-1":
								distanceStr = "-i"
							else:
								distanceStr = a + "i"
							if not b is None:
								distanceStr = b + "+" + distanceStr
						elif nType == "0^0":
							if random.choice([True, False]):
								distanceStr = "0⁰"
							else:
								distanceStr = "-0⁰"
							if not b is None:
								distanceStr += "+" + bHalf
						distanceStr = distanceStr.replace("+-", "-")
					#If not in the end
					else:
						#Convert X and Z coordinates to floats
						xPos = float(coords[0][:-1])
						zPos = float(coords[2][:-1])
						#If in the nether, multiply coordinates by 8
						if dimension == -1:
							xPos *= 8
							zPos *= 8
						#Calculate distance
						distance = math.sqrt(xPos**2 + zPos**2)
						#Convert to units of 500 m, then round and convert to km
						roundedDistance = round(distance/500)/2
						distanceStr = str(int(roundedDistance))
						if roundedDistance == 0.5:
							distanceStr = "½"
						elif roundedDistance % 1 == 0.5:
							distanceStr += "½"
					cmd("tellraw {} {{\"text\":\"You are {} km from the world's center\", \"color\":\"aqua\"}}".format(name, distanceStr))
			else:
				if not rconPattern.match(line) is None:
					mcr.connect()
					connected = True
		time.sleep(delay)
