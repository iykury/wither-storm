#Get the player's X or Z distance from the world's center, whichever is greater
execute as @a store result score @s playerx run data get entity @s Pos[0] 8 
execute as @a if score @s playerx < zero constants run scoreboard players operation @s playerx *= negativeone constants
execute as @a store result score @s playerz run data get entity @s Pos[2] 8
execute as @a if score @s playerz < zero constants run scoreboard players operation @s playerz *= negativeone constants
execute as @a if score @s playerx >= @s playerz run scoreboard players operation @s playercoord = @s playerx
execute as @a if score @s playerz >= @s playerx run scoreboard players operation @s playercoord = @s playerz

#Get the world border's radius
execute store result score radius borderradius run worldborder get
scoreboard players operation radius borderradius /= two constants

#Get player's dimension
execute as @a store result score @s dimension run data get entity @s Dimension

#Find if the player would be outside of the world border
scoreboard players set @a outsideborder 0
execute as @a[scores={dimension=-1}] if score @s playercoord >= radius borderradius run scoreboard players set @s outsideborder 1
title @a[scores={outsideborder=1}] times 0 5 0
title @a[scores={outsideborder=1}] title {"text":"WARNING!", "color": "red"}
title @a[scores={outsideborder=1}] subtitle {"text":"You are outside the overworld border!", "color": "yellow"}
