scoreboard objectives add playerx dummy
scoreboard objectives add playerz dummy
scoreboard objectives add playercoord dummy
scoreboard objectives add borderradius dummy
scoreboard objectives add outsideborder dummy
scoreboard objectives add dimension dummy

#Constants
scoreboard objectives add constants dummy
scoreboard players set zero constants 0
scoreboard players set negativeone constants -1
scoreboard players set two constants 2
