#!/usr/bin/env python3
import discord, os, asyncio, re, mcrcon, time

client = discord.Client()

filepath = os.path.abspath(__file__)
folder = os.path.dirname(filepath)
os.chdir(folder)

config = {}
try:
	with open("config") as configFile:
		for line in configFile.readlines():
			splitLine = line.split("=", 1)
			if len(splitLine) == 2:
				config[splitLine[0]] = splitLine[1].strip("\n")
except:
	print("Couldn't read config file")


token = config.get("token")
if token is None:
	print("Couldn't get token")

try:
	channelId = int(config.get("channelId"))
except:
	print("Couldn't get channel id")

logfile = config.get("logfile")
if logfile is None:
	print("Couldn't get logfile")

rconHost = config.get("rconHost")
if rconHost is None:
	print("Couldn't get RCON hostname")

rconPass = config.get("rconPass")
if rconPass is None:
	print("Couldn't get RCON password")

rconPort = config.get("rconPort")
if rconPort is None:
	rconPort = 25575

try:
	opRoleId = int(config.get("opRoleId"))
except:
	print("Couldn't get operator role id")

channel = None
opRole = None

def cmd(command):
	now = time.gmtime()
	print("[{}] {}".format(time.strftime("%H:%M:%S"), command))
	resp = mcr.command(command)
	print(resp)
	return resp

mcr = mcrcon.MCRcon(rconHost, rconPass, rconPort)
mcr.connect()
connected = True

@client.event
async def on_ready():
	print("Logged in as")
	print(client.user.name)
	print(client.user.id)
	print("------")

	global channel
	channel = client.get_channel(channelId)
	global opRole
	opRole = channel.guild.get_role(opRoleId)

	await mainLoop()

@client.event
async def on_message(message):
	#Bot won't reply to itself
	if message.author == client.user:
		return
	
	if message.channel == channel:
		if message.content.startswith("/"):
			if opRole in message.author.roles:
				resp = cmd(message.content)
				if resp != "":
					#1998 instead of 2000 so asterisks can be put on each end to make it italics
					chunks = (resp[0+i:1998+i] for i in range(0, len(resp), 1998)) #from stackoverflow because ofc it is
					for chunk in chunks:
						await channel.send("*{}*".format(chunk))
			else:
				await channel.send("*You are not an operator*")
		else:
			msg = message.clean_content.replace("\"", "\\\"") #Escape quotes so they don't break the JSON
			cmd("tellraw @a {{\"text\":\"<{}> {}\",\"color\":\"blue\"}}".format(message.author.name, msg))

def getLines(filename):
	with open(filename, "r") as f:
		lines = f.readlines()
	if lines[-1:] == "\n":
		lines = lines[:-1]
	return lines

async def mainLoop():
	global connected
	stopPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: Stopping server$")
	rconPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[RCON Listener #\\d+/INFO]: RCON running on \\d+\\.\\d+\\.\\d+\\.\\d+:\\d+$")
	delay = 1
	new = getLines(logfile)
	while True:
		old = new
		try:
			new = getLines(logfile)
		except FileNotFoundError:
			pass
		if len(new) > len(old):
			newLines = new[len(old):]
		elif len(new) < len(old):
			newLines = new
		else:
			newLines = []
		
		#Filter lines
		response = ""
		for line in newLines:
			#Filter out warning messages, etc.
			if line[11:31] == "[Server thread/INFO]":
				#Big regex that matches join messages that reveal a player's ip adress and in-game coordinates
				#We don't want that stuff to be public so this is to filter it out
				pattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: .+\\[/\\d+\\.\\d+\\.\\d+\\.\\d+:\\d+\\] logged in with entity id \\d+ at \\(-*\\d+\\.\\d+, -*\\d+\\.\\d+, -*\\d+.\\d+\\)$")
				if pattern.match(line) is None:
					#If the line passes both of those tests, add it to the message to be broadcast
					response += line[0:10] + line[32:]
			#If the server is shutting down, disconnect RCON
			if not stopPattern.match(line) is None:
				mcr.disconnect()
				connected = False
			if not connected:
				#If server is listening for RCON, reconnect
				if not rconPattern.match(line) is None:
					mcr.connect()
					connected = True
		if response != "":
			chunks = (response[0+i:2000+i] for i in range(0, len(response), 2000)) #from stackoverflow because ofc it is
			for chunk in chunks:
				await channel.send(chunk)
		await asyncio.sleep(delay)
	
client.run(token)
