#!/usr/bin/env python3
import time, mcrcon, os, math, re, random

filepath = os.path.abspath(__file__)
folder = os.path.dirname(filepath)
os.chdir(folder)

config = {}
try:
	with open("config") as configFile:
		for line in configFile.readlines():
			splitLine = line.split("=", 1)
			if len(splitLine) == 2:
				config[splitLine[0]] = splitLine[1].strip("\n")
except:
	print("Couldn't read config file")

logfile = config.get("logfile")
if logfile is None:
	print("Couldn't get logfile")

rconHost = config.get("rconHost")
if rconHost is None:
	print("Couldn't get RCON hostname")

rconPass = config.get("rconPass")
if rconPass is None:
	print("Couldn't get RCON password")

rconPort = config.get("rconPort")
if rconPort is None:
	rconPort = 25575

if not os.path.exists("armorstands"):
	os.mkdir("armorstands")

def cmd(command):
	now = time.gmtime()
	print("[{}] {}".format(time.strftime("%H:%M:%S"), command))
	resp = mcr.command(command)
	print(resp)
	return resp

def getLines(filename):
	with open(filename, "r") as f:
		lines = f.readlines()
	if lines[-1:] == "\n":
		lines = lines[:-1]
	return lines

killQueue = []

with mcrcon.MCRcon(rconHost, rconPass, rconPort) as mcr:
	connected = True
	delay = 1
	new = getLines(logfile)
	stopPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: Stopping server$")
	cmdPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[Server thread/INFO\\]: <.+> spectate$")
	rconPattern = re.compile("^\\[\\d\\d:\\d\\d:\\d\\d\\] \\[RCON Listener #\\d+/INFO]: RCON running on \\d+\\.\\d+\\.\\d+\\.\\d+:\\d+$")
	while True:
		old = new
		try:
			new = getLines(logfile)
		except FileNotFoundError:
			pass
		if len(new) > len(old):
			newLines = new[len(old):]
		elif len(new) < len(old):
			newLines = new
		else:
			newLines = []

		for line in newLines:
			if connected:
				#If the server is shutting down, disconnect
				if not stopPattern.match(line) is None:
					mcr.disconnect()
					connected = False
				#If a player types "spectate" in chat, either put them in or take them out of live spectator mode
				if not cmdPattern.match(line) is None:
					name = line[34:-11]
					#Get player's livespectator score
					resp = cmd("scoreboard players get {} livespectator".format(name))
					livespectator = int(resp[len(name)+5:-16])
					#If not in live spectator
					if livespectator == 0:
						#Get player's spectatorclock score
						resp = cmd("scoreboard players get {} spectatorclock".format(name))
						spectatorclock = int(resp[len(name)+5:-17])

						#Get player's withermusic score
						resp = cmd("scoreboard players get {} withermusic".format(name))
						withermusic = int(resp[len(name)+5:-14])

						#Get if there are any players to spectate
						resp = cmd("execute if entity @a[name=!{}, gamemode=!spectator]".format(name))
						otherPlayers = resp != "Test failed" #False if there aren't any players to spectate, true if there are

						#Get player's health
						resp = cmd("data get entity {} Health".format(name))
						health = float(resp[len(name)+32:-1])

						#Get player's air
						resp = cmd("data get entity {} Air".format(name))
						air = int(resp[len(name)+32:-1])

						#Get if player is in the air
						resp = cmd("execute at {} if block ~ ~-1 ~ air".format(name))
						inAir = resp == "Test passed" #True if the block below the player is air

						#Get player's damageclock score
						resp = cmd("scoreboard players get {} damageclock".format(name))
						damageclock = int(resp[len(name)+5:-14])

						if spectatorclock > 0:
							if random.randrange(100) == 0:
								cmd("tellraw {} {{\"text\": \"お前はもう死んでいる\", \"color\": \"red\"}}".format(name))
							else:
								cmd("tellraw {} {{\"text\": \"You are already dead\", \"color\": \"red\"}}".format(name))
						elif withermusic > 1:
							cmd("tellraw {} {{\"text\": \"You are too close to the Wither\", \"color\": \"red\"}}".format(name))
						elif not otherPlayers:
							cmd("tellraw {} {{\"text\": \"There are no other players to spectate\", \"color\": \"red\"}}".format(name))
						elif health < 20:
							cmd("tellraw {} {{\"text\": \"You are not at full health\", \"color\": \"red\"}}".format(name))
						elif air < 300:
							cmd("tellraw {} {{\"text\": \"You are underwater\", \"color\": \"red\"}}".format(name))
						elif inAir:
							cmd("tellraw {} {{\"text\": \"You are in the air\", \"color\": \"red\"}}".format(name))
						elif damageclock < 300:
							cmd("tellraw {} {{\"text\": \"You have taken damage recently\", \"color\": \"red\"}}".format(name))
						else:
							#Generate random UUID for armor stand
							uuid = random.getrandbits(128)

							#Get most and least significant 64 bits
							uuidMost = uuid >> 64
							uuidLeast = uuid % 2**64

							if uuidMost > 2**63-1:
								uuidMost -= 2**64
							if uuidLeast > 2**63-1:
								uuidLeast -= 2**64

							#Convert UUID to a string
							uuidHex = hex(uuid)[2:].zfill(32) #Convert to hex, remove "0x" at the beginning, and pad with zeros if necessary
							uuidStr = "-".join([uuidHex[:8], uuidHex[8:12], uuidHex[12:16], uuidHex[16:20], uuidHex[20:]]) #Split and join with hyphens

							#Summon armor stand at player's location
							cmd("execute at {0} run summon armor_stand ~ ~ ~ {{ArmorItems:[{{}},{{}},{{}},{{id:\"player_head\",Count:1b,tag:{{SkullOwner:\"{0}\"}}}}],NoBasePlate:1b,ShowArms:1b,Marker:1b,UUIDMost:{1}L, UUIDLeast:{2}L}}".format(name, uuidMost, uuidLeast))

							#Get player location, rotation, and dimension
							dimensionIds = {"0": "overworld", "-1": "the_nether", "1": "the_end"}
							resp = cmd("data get entity {} Pos".format(name))
							coords = [x[:-1] for x in resp[33+len(name):-1].split(", ")]
							resp = cmd("data get entity {} Rotation".format(name))
							rotation = [x[:-1] for x in resp[33+len(name):-1].split(", ")]
							resp = cmd("data get entity {} Dimension".format(name))
							dimension = dimensionIds[resp[32+len(name):]]

							#Put player in spectator by setting livespectator score to 1
							cmd("scoreboard players set {} livespectator 1".format(name))
							
							#Save necessary data to a file
							with open("armorstands/{}".format(name), "w") as f:
								f.write("x={}\ny={}\nz={}\nyaw={}\npitch={}\ndimension={}\nuuid={}".format(
									coords[0], coords[1], coords[2], rotation[0], rotation[1], dimension, uuidStr
								))
					#If in live spectator
					else:
						#Read file
						stand = {}
						try:
							with open("armorstands/{}".format(name), "r") as standFile:
								for line in standFile.readlines():
									splitLine = line.split("=", 1)
									if len(splitLine) == 2:
										stand[splitLine[0]] = splitLine[1].strip("\n")
						except:
							print("Couldn't read armorstand file")

						#Set player's livespectator score back to 0
						cmd("scoreboard players set {} livespectator 0".format(name))
						#Return player to survival
						cmd("scoreboard players set {} spectatorclock 2".format(name))

						#Teleport player to their original position
						cmd("execute in {} run tp {} {} {} {} {} {}".format(
							stand["dimension"], name, stand["x"], stand["y"], stand["z"], stand["yaw"], stand["pitch"]
						))

						#Add armor stand to kill queue
						killQueue.append({"uuid": stand["uuid"], "tries": 0})
						#Delete file
						os.remove("armorstands/{}".format(name))
			else:
				if not rconPattern.match(line) is None:
					mcr.connect()
					connected = True
		#Kill all armor stands in the kill queue
		for stand in killQueue:
			resp = cmd("kill {}".format(stand["uuid"]))
			stand["tries"] += 1
			#Remove stand from queue if the kill succeeds or if it's failed 10 times
			if resp == "Killed Armor Stand" or stand["tries"] >= 10:
				killQueue.remove(stand)

		time.sleep(delay)
