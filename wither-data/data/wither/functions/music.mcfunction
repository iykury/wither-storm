scoreboard players add @a withermusic 0

execute as @e[type=wither] at @s positioned ~-96 ~-128 ~-96 if entity @a[scores={withermusic=0},gamemode=!creative,gamemode=!spectator,dx=192,dy=256,dz=192] at @s run summon armor_stand ~ ~3 ~ {NoAI:1,Invulnerable:1,Invisible:1,Marker:1b,Tags:["withertrigger"]}
execute as @e[type=armor_stand,tag=withertrigger] at @s facing entity @p[scores={withermusic=0},gamemode=!creative,gamemode=!spectator] eyes run tp @s ^ ^ ^1
execute as @a[scores={withermusic=0},gamemode=!creative,gamemode=!spectator] at @s positioned ~ ~1.625 ~ if entity @e[tag=withertrigger,distance=0..1] run scoreboard players set @s withermusic 1
execute as @a[scores={withermusic=0},gamemode=!creative,gamemode=!spectator] at @s if entity @e[type=wither,distance=0..40] run scoreboard players set @s withermusic 1
execute as @e[tag=withertrigger] at @s unless block ~ ~ ~ #wither:clear run kill @s
execute as @e[tag=withertrigger] at @s positioned ~-96 ~-128 ~-96 unless entity @a[scores={withermusic=0},gamemode=!creative,gamemode=!spectator,dx=192,dy=256,dz=192] run kill @s
execute as @e[tag=withertrigger] at @s positioned ~-80 ~-128 ~-80 unless entity @e[type=wither,dx=160,dy=256,dz=160] run kill @s

execute as @a[scores={withermusic=1..}] at @s if entity @e[type=wither,distance=0..128] run scoreboard players add @s withermusic 1
scoreboard players set @a[scores={withermusic=3506..}] withermusic 2539
execute as @a[scores={withermusic=1..}] at @s positioned ~-80 ~-128 ~-80 unless entity @e[type=wither,dx=160,dy=256,dz=160] run scoreboard players set @s withermusic -2
scoreboard players add @a[scores={withermusic=-2..-1}] withermusic 1

scoreboard players set @a[gamemode=!survival,gamemode=!adventure,scores={withermusic=1..}] withermusic -2

#Stop any bg music already playing
execute as @a[scores={withermusic=1..}] at @s run stopsound @s * music.game
execute as @a[scores={withermusic=1..}] at @s run stopsound @s * music.creative
execute as @a[scores={withermusic=1..}] at @s run stopsound @s * music.under_water

execute as @a[scores={withermusic=2}] at @s run playsound music.wither.start music @s ~ ~ ~ 0.5
execute as @a[scores={withermusic=3505}] at @s run playsound music.wither.loop music @s ~ ~ ~ 0.5
execute as @a[scores={withermusic=-1}] at @s run playsound music.wither.end music @s ~ ~ ~ 0.5
execute as @a[scores={withermusic=-1}] at @s run stopsound @s * music.wither.start
execute as @a[scores={withermusic=-1}] at @s run stopsound @s * music.wither.loop

