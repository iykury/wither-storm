scoreboard objectives add wither dummy
scoreboard objectives add withermusic dummy
scoreboard players set summoned wither 0
scoreboard players set sleep wither 0

scoreboard objectives add witherrage dummy
scoreboard objectives add witherskullage dummy
scoreboard objectives add witherminions dummy
scoreboard objectives add dontkill dummy

scoreboard objectives add witherhealth dummy
scoreboard objectives add multiplyready dummy