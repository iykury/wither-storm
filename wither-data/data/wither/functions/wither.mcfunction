execute as @e[type=wither] at @s facing entity @p[gamemode=!creative,gamemode=!spectator] feet run tp @s ^ ^ ^0.01
execute as @e[type=wither] at @s run forceload add ~-64 ~-64 ~64 ~64
scoreboard players add sleep wither 1
execute in overworld if score sleep wither matches 200 unless entity @e[type=wither] at @r store success score summoned wither run summon wither ~ 255 ~ {Invul:1}
execute if score sleep wither matches 200 run scoreboard players set sleep wither 0
execute if score summoned wither matches 1 run spreadplayers ~ ~ 0 2048 true @e[type=wither]
scoreboard players set summoned wither 0

tag @a[scores={withermusic=1..}] add withertarget
tag @a[scores={withermusic=..0}] remove withertarget
execute as @a at @s anchored eyes positioned ^ ^ ^1 if block ~ ~ ~ #wither:utility unless entity @e[type=area_effect_cloud,tag=withertarget,distance=0..6.2] run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @a at @s anchored eyes positioned ^ ^ ^2 if block ~ ~ ~ #wither:utility unless entity @e[type=area_effect_cloud,tag=withertarget,distance=0..6.2] run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @a at @s anchored eyes positioned ^ ^ ^3 if block ~ ~ ~ #wither:utility unless entity @e[type=area_effect_cloud,tag=withertarget,distance=0..6.2] run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @a at @s anchored eyes positioned ^ ^ ^4 if block ~ ~ ~ #wither:utility unless entity @e[type=area_effect_cloud,tag=withertarget,distance=0..6.2] run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @a at @s anchored eyes positioned ^ ^ ^5 if block ~ ~ ~ #wither:utility unless entity @e[type=area_effect_cloud,tag=withertarget,distance=0..6.2] run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @e[type=area_effect_cloud,tag=withertarget] at @s unless block ~ ~ ~ #wither:utility run kill @s
execute as @e[type=armor_stand,tag=withertarget] at @s run summon area_effect_cloud ~ ~ ~ {Duration:999999999,Radius:0,Tags:["withertarget"]}
execute as @e[type=armor_stand,tag=withertarget] run kill @s

execute as @e[type=wither] at @s if entity @a[distance=0..40,gamemode=!creative,gamemode=!spectator] unless entity @e[tag=witherreticle,distance=0..128] run summon silverfish ^ ^ ^ {NoAI:1,Tags:["witherreticle"],Silent:1b,ActiveEffects:[{Id:14,Amplifier:0,Duration:20000000,ShowParticles:0b}]}
execute as @e[type=wither] at @s if entity @e[tag=withertarget,distance=0..40] unless entity @e[tag=witherreticle,distance=0..128] run summon silverfish ^ ^ ^ {NoAI:1,Tags:["witherreticle"],Silent:1b,ActiveEffects:[{Id:14,Amplifier:0,Duration:20000000,ShowParticles:0b}]}
execute as @e[tag=witherreticle] at @s facing entity @e[tag=withertarget,limit=1,sort=nearest] eyes unless entity @e[tag=withertarget,distance=0..2,type=player] if block ^ ^ ^0.5 #wither:clear run tp @s ^ ^ ^0.5
execute as @e[tag=witherreticle] at @s unless block ~ ~ ~ #wither:clear run tp @s ~ ~0.5 ~
execute as @e[tag=witherreticle] at @s unless entity @e[type=wither,distance=0..32] run kill @s
execute as @e[tag=witherreticle] at @s unless entity @e[tag=withertarget,distance=0..128] run kill @s
scoreboard players add @e[tag=witherreticle] witherskullage 1
execute as @e[tag=witherreticle,scores={witherskullage=100..}] at @s facing entity @e[tag=withertarget,limit=1,sort=nearest] eyes unless block ^ ^ ^0.5 #wither:clear run kill @s
execute as @e[tag=witherreticle,scores={witherskullage=300..}] run kill @s
execute as @e[tag=witherreticle] at @s facing entity @e[tag=withertarget,limit=1,sort=nearest] eyes unless block ^ ^ ^0.5 #wither:clear run scoreboard players add @e[type=wither,limit=1,sort=nearest] witherrage 1
execute as @e[tag=witherreticle] at @s if entity @e[type=wither_skull,distance=0..2] run data modify entity @s Invulnerable set value 1
execute as @e[tag=witherreticle] at @s unless entity @e[type=wither_skull,distance=0..2] run data modify entity @s Invulnerable set value 0

execute as @e[type=wither,scores={witherrage=100..}] at @s run summon tnt ^ ^ ^ {Fuse:100,Tags:["withertnt"]}
execute as @e[type=wither,scores={witherrage=100..}] run scoreboard players set @s witherrage 0
scoreboard players add @e[tag=withertnt] witherskullage 1
execute as @e[tag=withertnt,scores={witherskullage=1..20}] at @s facing entity @e[tag=witherreticle,distance=0..128] eyes if block ^ ^ ^0.25 #wither:clear run tp @s ^ ^ ^0.25
execute as @e[type=wither] at @s unless block ~ ~4 ~ #wither:clear run tp @e[tag=withertnt,scores={witherskullage=0..21}] ~ ~5 ~
execute as @e[tag=withertnt,scores={witherskullage=99}] at @s run tp @s ~ ~-1 ~
execute as @e[type=wither] at @s if entity @e[tag=withertnt,scores={witherskullage=99},distance=0..8] run data modify entity @s Invulnerable set value 1
execute as @e[type=wither] at @s unless entity @e[tag=withertnt,scores={witherskullage=99..},distance=0..8] run data modify entity @s Invulnerable set value 0

scoreboard players set @e[type=wither_skull] dontkill 0
execute as @e[type=wither_skull] at @s if entity @a[distance=0..128,gamemode=!spectator] run scoreboard players set @s dontkill 1
execute as @e[type=wither_skull] at @s if entity @e[distance=0..48,type=wither] run scoreboard players set @s dontkill 1
kill @e[type=wither_skull,scores={dontkill=0}]

execute as @e[type=wither] at @s run forceload remove ~-80 ~-64 ~-80 ~80
execute as @e[type=wither] at @s run forceload remove ~-64 ~80 ~80 ~80
execute as @e[type=wither] at @s run forceload remove ~80 ~64 ~80 ~-80
execute as @e[type=wither] at @s run forceload remove ~64 ~-80 ~-80 ~-80

execute as @e[tag=witherreticle] at @s if block ~ ~-0.5 ~ #wither:clear run scoreboard players add @e[type=wither,limit=1,sort=nearest] witherminions 1
execute as @e[type=wither,scores={witherminions=400}] at @a[scores={withermusic=1..},limit=1,sort=nearest] facing entity @s feet run summon armor_stand ^ ^ ^-16 {Tags:["witherlightning"],Invisible:1}
execute as @e[tag=witherlightning] at @s run spreadplayers ~ ~ 2 1 false @s
execute as @e[tag=witherlightning] at @s run summon lightning_bolt ~ ~ ~
execute as @e[tag=witherlightning] at @s run summon wither_skeleton ~ ~ ~ 
execute as @e[tag=witherlightning] at @s run summon wither_skeleton ~ ~ ~ 
execute as @e[tag=witherlightning] at @s run summon wither_skeleton ~ ~ ~ {HandItems:[{Count:1,id:bow}]}
execute as @e[tag=witherlightning] at @s run summon wither_skeleton ~ ~ ~ {HandItems:[{Count:1,id:bow}]}
execute as @e[tag=witherlightning] at @s run kill @s
execute as @e[type=wither,scores={witherminions=400..}] run scoreboard players set @s witherminions 0

execute in the_nether as @a[scores={withermusic=..-1}] at @s if block ~ ~1 ~ nether_portal run summon wither ~ ~ ~ {Invul:200,Tags:["netherstrander"]}
execute as @e[type=wither,tag=netherstrander,nbt={Invul:0}] run kill @s

execute as @e[type=wither] store result score @s witherhealth run data get entity @s Health
scoreboard players set @e[type=wither,scores={witherhealth=300}] multiplyready 1
execute as @e[type=wither,scores={multiplyready=1,witherhealth=..150}] at @s run summon wither ~ ~ ~ {Invul:200,Health:90f}
scoreboard players set @e[type=wither,scores={witherhealth=..150}] multiplyready 0