bossbar add respawntimer:0 "Ghost Mode"
bossbar set respawntimer:0 color yellow
bossbar set respawntimer:0 max 600
bossbar set respawntimer:0 style notched_10

bossbar add respawntimer:1 "Live Ghost Mode"
bossbar set respawntimer:1 color green
bossbar set respawntimer:1 max 600
bossbar set respawntimer:1 style notched_10

scoreboard objectives add rtdeaths deathCount
scoreboard objectives add spectatorclock dummy
scoreboard objectives add livespectator dummy

scoreboard objectives add currenthealth health
scoreboard objectives add previoushealth dummy
scoreboard objectives add damageclock dummy
