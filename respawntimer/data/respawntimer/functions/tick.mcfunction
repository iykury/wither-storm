scoreboard players add @a livespectator 0
scoreboard players set @a[scores={spectatorclock=1..},gamemode=creative] spectatorclock 0
scoreboard players set @a[scores={rtdeaths=1..},gamemode=creative] rtdeaths 0
scoreboard players set @a[scores={rtdeaths=1..}] spectatorclock 600
scoreboard players set @a[scores={livespectator=1}] spectatorclock 600
scoreboard players set @a[scores={rtdeaths=1..}] rtdeaths -1
scoreboard players remove @a[scores={spectatorclock=1..,livespectator=0}] spectatorclock 1
gamemode spectator @a[scores={spectatorclock=1..,currenthealth=1..},gamemode=!spectator,gamemode=!creative]
scoreboard players remove @a[scores={spectatorclock=1,rtdeaths=-1},gamemode=spectator] Deaths 1
kill @a[scores={spectatorclock=1,rtdeaths=-1},gamemode=spectator]
gamemode survival @a[scores={spectatorclock=1,rtdeaths=0,currenthealth=1..},gamemode=spectator]
bossbar set respawntimer:0 players @a[scores={spectatorclock=1..livespectator=0},gamemode=spectator]
bossbar set respawntimer:1 players @a[scores={livespectator=1},gamemode=spectator]
execute as @a[gamemode=spectator,scores={livespectator=0}] store result bossbar respawntimer:0 value run scoreboard players get @s spectatorclock
execute as @a[gamemode=spectator,scores={livespectator=1}] store result bossbar respawntimer:1 value run scoreboard players get @s spectatorclock
execute as @a[gamemode=spectator,scores={spectatorclock=1..}] at @s unless entity @a[gamemode=!spectator,distance=0..9.51] at @p[gamemode=!spectator] facing entity @s feet run tp @s ^ ^ ^9.5
execute as @a[gamemode=spectator,scores={spectatorclock=1..}] at @s anchored eyes unless block ^ ^ ^ #respawntimer:clear run effect give @s blindness 69420 1 true
execute as @a[gamemode=spectator,scores={spectatorclock=1..}] at @s anchored eyes if block ^ ^ ^ #respawntimer:clear run effect clear @s blindness

#Damage clock
scoreboard players add @a damageclock 1
execute as @a if score @s previoushealth > @s currenthealth run scoreboard players set @s damageclock 0
execute as @a store result score @s previoushealth run scoreboard players get @s currenthealth
