#tag @e[type=#witherpvp:villagers] add wsptarget
tag @a[team=!undead,gamemode=!spectator,gamemode=!creative] add wsptarget
tag @a[team=undead] remove wsptarget
tag @a[gamemode=spectator] remove wsptarget
tag @a[gamemode=creative] remove wsptarget

execute as @a[team=undead] at @s unless entity @e[tag=orienter,distance=..1] run summon armor_stand ~ ~ ~ {Invisible:1,Marker:1b,Tags:["orienter"]}
execute as @a[team=undead] at @s if entity @e[tag=orienter,distance=..1] run tp @e[tag=orienter,sort=nearest,limit=1] @s
execute as @e[tag=orienter,nbt={Dimension:-1}] at @s facing entity @e[tag=wsptarget,nbt={Dimension:-1},limit=1,sort=nearest] feet run tp @s ~ ~ ~ ~ ~
execute as @e[tag=orienter,nbt={Dimension:0}] at @s facing entity @e[tag=wsptarget,nbt={Dimension:0},limit=1,sort=nearest] feet run tp @s ~ ~ ~ ~ ~
execute as @e[tag=orienter,nbt={Dimension:1}] at @s facing entity @e[tag=wsptarget,nbt={Dimension:1},limit=1,sort=nearest] feet run tp @s ~ ~ ~ ~ ~

execute as @a[team=undead] store result score @s orientation run data get entity @s Rotation[0]
execute as @a[team=undead] store result score @e[tag=orienter,sort=nearest,limit=1] orientation run data get entity @e[tag=orienter,sort=nearest,limit=1] Rotation[0]
execute as @a[team=undead] run scoreboard players operation @s orientation -= @e[tag=orienter,sort=nearest,limit=1] orientation
execute as @a[scores={orientation=..-180}] run scoreboard players add @s orientation 360
execute as @a[scores={orientation=180..}] run scoreboard players remove @s orientation 360

title @a[scores={orientation=166..,wsptimer=70}] actionbar {"text":"<<<+>>>","color":"light_purple"}
title @a[scores={orientation=136..165,wsptimer=70}] actionbar {"text":"<<<<<","color":"red"}
title @a[scores={orientation=106..135,wsptimer=70}] actionbar {"text":"<<<<","color":"gold"}
title @a[scores={orientation=76..105,wsptimer=70}] actionbar {"text":"<<<","color":"yellow"}
title @a[scores={orientation=46..75,wsptimer=70}] actionbar {"text":"<<","color":"green"}
title @a[scores={orientation=16..45,wsptimer=70}] actionbar {"text":"<","color":"aqua"}
title @a[scores={orientation=-15..15,wsptimer=70}] actionbar {"text":"O","color":"blue"}
title @a[scores={orientation=-45..-16,wsptimer=70}] actionbar {"text":">","color":"aqua"}
title @a[scores={orientation=-75..-46,wsptimer=70}] actionbar {"text":">>","color":"green"}
title @a[scores={orientation=-105..-76,wsptimer=70}] actionbar {"text":">>>","color":"yellow"}
title @a[scores={orientation=-135..-106,wsptimer=70}] actionbar {"text":">>>>","color":"gold"}
title @a[scores={orientation=-165..-136,wsptimer=70}] actionbar {"text":">>>>>","color":"red"}
title @a[scores={orientation=..-166,wsptimer=70}] actionbar {"text":"<<<+>>>","color":"light_purple"}
execute unless entity @e[tag=wsptarget,nbt={Dimension:-1}] run title @a[team=undead,scores={wsptimer=70},nbt={Dimension:-1}] actionbar {"text":"No available targets","color":"red"}
execute unless entity @e[tag=wsptarget,nbt={Dimension:0}] run title @a[team=undead,scores={wsptimer=70},nbt={Dimension:0}] actionbar {"text":"No available targets","color":"red"}
execute unless entity @e[tag=wsptarget,nbt={Dimension:1}] run title @a[team=undead,scores={wsptimer=70},nbt={Dimension:1}] actionbar {"text":"No available targets","color":"red"}

