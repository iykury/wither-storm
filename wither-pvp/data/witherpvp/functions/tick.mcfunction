execute as @a[team=undead,gamemode=adventure,scores={hunger=..17}] run effect give @s saturation 999999 0 true
execute as @a[team=undead,gamemode=adventure,scores={hunger=19..}] run effect give @s hunger 999999 0 true
execute as @a[team=undead,gamemode=adventure,scores={hunger=18..}] run effect clear @s saturation
execute as @a[team=undead,gamemode=adventure,scores={hunger=..18}] run effect clear @s hunger
execute as @a[team=undead,gamemode=adventure] run effect give @s fire_resistance 999999 0 true
execute as @a[team=undead,gamemode=adventure] run effect give @s night_vision 999999 0 true

execute at @a[nbt={SelectedItem:{tag:{wither:1}}},scores={attack=1..}] run effect give @e[nbt={HurtTime:10s},sort=nearest] wither 10 0 false
scoreboard players set @a attack 0

execute as @e[type=#witherpvp:undead] at @s if entity @a[team=undead,distance=0..64] run team join undead @s

scoreboard players add @a[team=undead,scores={wsptimer=..69}] wsptimer 1
execute if entity @a[team=undead] run function witherpvp:orient
execute as @e[tag=orienter] at @s unless entity @a[team=undead,distance=..1] run kill @s

execute as @a[team=undead,nbt=!{Inventory:[{Slot:103b,id:"minecraft:wither_skeleton_skull"}]},scores={health=1..}] run function witherpvp:items