team join undead @s
gamemode adventure @s
title @s title ""
title @s subtitle {"text":"You are now a Wither Skeleton","color":"red"}
title @s actionbar {"text":"Kill other players to regain your freedom","color":"white"}
replaceitem entity @s hotbar.0 stone_sword{display:{Name:"\"Wither Sword\"",Lore:["\"Wither I\""]},Enchantments:[{id:sharpness,lvl:7},{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}],wither:1} 1
replaceitem entity @s hotbar.1 bow{display:{Name:"\"Wither Bow\""},Enchantments:[{id:flame,lvl:1},{id:infinity,lvl:1},{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}]} 1
replaceitem entity @s inventory.0 arrow 1
#replaceitem entity @s weapon.offhand shield{display:{Name:"\"Wither Shield\""},Enchantments:[{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}],BlockEntityTag:{Base:11,Patterns:[{Pattern:"gru",Color:14},{Pattern:"cre",Color:15},{Pattern:"sku",Color:15},{Pattern:"bt",Color:10},{Pattern:"bts",Color:1}]}} 1
replaceitem entity @s weapon.offhand shield{display:{Name:"\"Wither Shield\""},Enchantments:[{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}]} 1
replaceitem entity @s armor.head wither_skeleton_skull{Enchantments:[{id:vanishing_curse,lvl:1},{id:binding_curse,lvl:1}]} 1
replaceitem entity @s armor.chest leather_chestplate{display:{color:1908001},Enchantments:[{id:protection,lvl:4},{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}]} 1
replaceitem entity @s armor.legs leather_leggings{display:{color:1908001},Enchantments:[{id:protection,lvl:4},{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}]} 1
replaceitem entity @s armor.feet leather_boots{display:{color:1908001},Enchantments:[{id:protection,lvl:4},{id:unbreaking,lvl:10},{id:mending,lvl:1},{id:vanishing_curse,lvl:1}]} 1
scoreboard players set @s wsptimer 1
execute at @r[team=!undead,nbt={Dimension:0}] run spreadplayers ~ ~ 0 1000 false @s